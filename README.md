# WikiSuite Packages

Script and packages for an optimal setup for WikiSuite components, especially [Virtualmin](https://www.virtualmin.com/), [Tiki Manager](https://gitlab.com/tikiwiki/tiki-manager) and [Tiki Wiki CMS Groupware](https://gitlab.com/tikiwiki/tiki).

This reuses the standard [Virtualmin installation script](https://github.com/virtualmin/virtualmin-install/blob/master/virtualmin-install.sh) and will make opiniated choices that make sense for running Tiki instances. For example, it will install Tiki dependencies and multiple versions of PHP. Differences are explained [here](https://wikisuite.org/Differences-from-the-original-script). After running the script, you can configure Webmin/Virtualmin as usual. 


# Requirements
- A special thank you to [Gitlab-buildpkg-tools](https://gitlab.com/Orange-OpenSource/gitlab-buildpkg-tools), which permits to manage packages for several Linux distros using [GitLab CI](https://about.gitlab.com/stages-devops-lifecycle/continuous-integration/).
- And thank you as well to GitLab for hosting [https://packages.wikisuite.org](https://packages.wikisuite.org).
- WikiSuite relies on [Virtualmin, which supports several Operating Systems](https://www.virtualmin.com/os-support/). Please see [historical context](https://wikisuite.org/blogpost16-WikiSuite-will-now-support-all-major-Linux-distros).

And thus, WikiSuite supports a subset of operating systems both supported by Gitlab-buildpkg-tools and Virtualmin.

## Supported
- Debian 12: Supported
- Debian 11: Reference implementation
- Debian 10: Also supported

## Experimental
- Ubuntu 20.04 and 22.04 LTS
- [NGINX](https://wikisuite.org/NGINX)

## On roadmap
- Alma, Rocky and RHEL 9

## Not planned
- CentOS 7, Alma/Rocky/RHEL 8: Support period is nearing the end
- CentOS Stream: [Depends on the Virtualmin project. Please see forums for latest discussions](https://forum.virtualmin.com/search?expanded=true&q=centos%20stream%20order%3Alatest). It appears [there is an attempt to support CentOS Stream 8 and 9](https://github.com/virtualmin/virtualmin-install/commit/8e60b3113926e05423885a9a66fe2e0b1cce28f5), but it's [unclear if it will remain as unofficial](https://github.com/virtualmin/virtualmin-install/commit/b7fb433d07356eb7597c97cb955416d18b656eeb).


# How to use
Please see https://wikisuite.org/How-to-install-WikiSuite

# Development / Contributing

Clone the repository main repository https://gitlab.com/wikisuite/wikisuite-packages
to your gitlab account, do your changes in that clone, and then open a merge request
when the changes are ready to be incorporated in wikisuite for review.

To test your changes before merging you have 2 possibilities:

* If the pipeline run and the gitlab CI job `pages:deploy` was sucessfull, you now have a APT
repository hosted in a gitlab url that you can use to install WikiSuite with
`wikisuite-installer -d https://example-url-for-project.gitlab.io`
* In alternative, you can download the zip generated as artifact by the gitlab CI job
`generatePages` and uncompress it on a local folder in the server, now you can force
the installer to use that local folder with `wikisuite-installer -r /path/to/folder`

# See also
- https://gitlab.com/Orange-OpenSource/gitlab-buildpkg-tools/-/issues/32
- https://wikisuite.org/Create-Own-GitLab-Testing-Environment-For-wikisuite-packages
